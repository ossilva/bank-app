(ns ossilva.util
  (:require [clojure.data.json :as json]
            [ossilva.bank :as bank]
            [ossilva.server :as server]))

(def server-test-port 3000)

(defn server-fixture  [f]
  (try
    (server/start)
    (f)
    (finally (server/stop))))

(def test-account-data
  {:a {:name "Mr. Allisson"}
   :b {:name "Ms. Bobson"}
   :c {:name "Mr. Carlton"}})

(defn create-accounts [node]
  (vec (for [[_ {:keys [name]}] test-account-data
             :let [{:keys [account-number]}
                   (bank/create-account node name)]]
         account-number)))

(defn test-url-path [& args]
  (apply str "http://localhost:" server-test-port args))

(defn req [method req-body & path-parts]
  (-> @(method (apply test-url-path path-parts)
               {:headers {"Content-Type"
                          "application/json; charset=utf-8"}
                :body (json/write-str req-body)
                :as :text})
      (update :body #(json/read-str % :key-fn keyword))))
