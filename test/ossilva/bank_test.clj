(ns ossilva.bank-test
  (:require [clojure.test :refer [deftest testing is use-fixtures]]
            [xtdb.api :as xt]
            [ossilva.bank :as bank]
            [ossilva.bank-db :as db]
            [ossilva.util :as util]))

;; node symbol bound in fixture
(def node* (atom nil))

(use-fixtures :each (fn [f]
                      (with-open [node (xt/start-node {})]
                        (db/setup! node)
                        (reset! node* node)
                        (f))))

(defn get-entity! [node entity-id]
  (xt/entity (xt/db node) entity-id))

(deftest bank-usage-happy-path-test
  (testing "accounts can be created"
    (let [accs (util/create-accounts @node*)]
      (is (= 3 (count (map (partial get-entity! @node*) accs))))))
  (testing "account transactions"
    (testing "deposit, transfer, withdraw"
      (let [[a b c] (util/create-accounts @node*)]
        (bank/deposit-money @node* a 100)
        (bank/deposit-money @node* c 10) ; a does not participate
        (bank/transfer-money @node* a b 5)
        (bank/transfer-money @node* c a 10)
        (bank/withdraw-money @node* a 20)
        (testing "JSON is equivalent to example (c deposit does not appear)"
          (let [expected-audit-log [{:sequence 3
                                     :debit 20
                                     :description "withdraw"}
                                    {:sequence 2
                                     :credit 10
                                     :description (str "receive from " c)}
                                    {:sequence 1
                                     :debit 5
                                     :description (str "send to " b)}
                                    {:sequence 0
                                     :credit 100
                                     :description "deposit"}]
                audit-json (bank/audit-log @node* a)]
            (is (= expected-audit-log audit-json))))))))

(deftest bank-txs-individually-test
  (let [[a b c]
        (util/create-accounts @node*)

        [a-acc b-acc _c-acc]
        [(assoc (:a util/test-account-data) :account-number a)
         (assoc (:b util/test-account-data) :account-number b)
         (assoc (:c util/test-account-data) :account-number c)]]
    (testing "account transactions reflect new situation"
      (testing "deposit"
        (is (= (assoc a-acc :balance 0) (bank/get-account-summary @node* a)))
        (is (= (assoc a-acc :balance 100) (bank/deposit-money @node* a 100))))
      (testing "transfer"
        (is (= (assoc a-acc :balance 100) (bank/get-account-summary @node* a)))
        (is (= (assoc a-acc :balance 0) (bank/transfer-money @node* a b 100))))
      (testing "withdraw"
        (is (assoc b-acc :balance 100) (bank/get-account-summary @node* b))
        (is (= (assoc b-acc :balance 0) (bank/withdraw-money @node* b 100)))))))

(deftest bank-usage-constraints-test
  (testing "failing when"
    (testing "result: negative source account balance"
      (let [[a] (util/create-accounts @node*)]
        (is (thrown? Exception (bank/withdraw-money @node* a 10)))))
    (testing "transfer amount is negative"
      (let [[a b] (util/create-accounts @node*)]
        (bank/deposit-money @node* a 10)
        (bank/deposit-money @node* b 10)
        (is (thrown? Exception (bank/transfer-money @node* a b -10)))))
    (testing "deposit amount is zero"
      (let [[a] (util/create-accounts @node*)]
        (is (thrown? Exception (bank/deposit-money @node* a 0)))))))
