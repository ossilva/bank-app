(ns ossilva.server-test
  (:require [clojure.test :refer [deftest testing is use-fixtures]]
            [org.httpkit.client :as http]
            [ossilva.util :as util]))


(use-fixtures :each util/server-fixture)

(defn- req-body [method req-body & path-parts]
  (let [{:keys [body status]}
        (apply util/req method req-body path-parts)]
    (is (= status 200))
    body))

(deftest test-api
  (testing "example sequence bodies match after"
    (let [[a b c] (vals util/test-account-data)
          {acc-a :account-number :as body} (req-body http/post a "/account")
          {acc-b :account-number} (req-body http/post b "/account")
          {acc-c :account-number} (req-body http/post c "/account")]
      (testing "account creation"
        (is (= (assoc a
                      :account-number acc-a
                      :balance 0) body)))
      (testing "deposit"
        (let [_ (req-body http/post {:amount 1000} "/account/" acc-c "/deposit")
              deposit-body (req-body http/post {:amount 100} "/account/" acc-a "/deposit")]
          (is (= (assoc a
                        :account-number acc-a
                        :balance 100) deposit-body))))
      (testing "transfer from to b to a"
        (let [body (req-body http/post {:amount 5
                                        :account-number acc-b} "/account/" acc-a "/send")]
          (is (= (assoc a
                        :account-number acc-a
                        :balance 95) body))))
      (testing "transfer from a to c"
        (let [body (req-body http/post {:amount 10
                                        :account-number acc-a} "/account/" acc-c "/send")]
          (is (= (assoc c
                        :account-number acc-c
                        :balance 990) body))))
      (testing "withdraw"
        (let [body (req-body http/post {:amount 20} "/account/" acc-a "/withdraw")]
          (is (= (assoc a
                        :account-number acc-a
                        :balance 85) body)))))))

