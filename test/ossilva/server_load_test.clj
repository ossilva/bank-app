(ns ossilva.server-load-test
  (:require [clojure.test :refer [deftest testing is use-fixtures]]
            [clj-gatling.core :as gtl]
            [org.httpkit.client :as http]
            [ossilva.util :as util]))


(def test-deposit-amount 123)
(def test-acc-name  "Mr. Black")

(use-fixtures :each util/server-fixture)

(defn create-account-request [context]
  (let [{{:keys [account-number]} :body :keys [status] :as res}
        (util/req http/post
                  {:name test-acc-name}
                  "/account")]
    [(= status 200) (assoc context :acc-id account-number :acc-name test-acc-name)]))

(defn account-deposit-request [context]
  (let [{{:keys [balance]} :body :keys [status]}
        (util/req http/post {:amount test-deposit-amount}
                  "/account/" (:acc-id context) "/deposit")]
    [(and (= status 200) (= test-deposit-amount balance)) (assoc context :deposit-amount balance)]))

(defn view-account-request [context]
  (let [{{:keys [balance account-number]} :body :keys [status]}
        (util/req http/get {} "/account/" (:acc-id context))]
    (and (= status 200) (= balance test-deposit-amount) (= (:acc-id context) account-number))))

;; Sends 1000 sequences of http requests in parallel to the API.
(deftest load-test-server
  (testing "simulate basic account usage"
    (gtl/run
     {:name "account creation"
      :scenarios [{:name "Testing creation and deposit"
                   :steps [{:name "Create account"
                            :request create-account-request}
                           {:name "Deposit to account"
                            :request account-deposit-request}
                           {:name "View account info"
                            :request view-account-request}]}]}
     {:concurrency 1000})))
