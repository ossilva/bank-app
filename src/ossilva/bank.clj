(ns ossilva.bank
  (:require [clojure.set :refer [rename-keys]]
            [clojure.core.match :refer [match]]
            [ossilva.bank-db :as db]))

(defn un-ns-keys [m]
  (update-keys m (comp keyword name)))

(def xtdb-data->api-data (comp un-ns-keys
                               #(rename-keys % {:xt/id :account-number})))

;; bank actions (public)

(defn get-account-summary [node account-id]
  (xtdb-data->api-data (db/sync-get-entity! node account-id)))

(defn create-account [node name]
  (xtdb-data->api-data (db/sync-put-account! node {:account-name name})))

(defn deposit-money [node account-id amount]
  (when (db/sync-transfer! node {:sender-id db/global-reserve-id
                                 :recipient-id account-id
                                 :amount amount
                                 :kind :deposit})
    (get-account-summary node account-id)))

(defn withdraw-money [node account-id amount]
  (when (db/sync-transfer! node {:sender-id account-id
                                 :recipient-id db/global-reserve-id
                                 :amount amount
                                 :kind :withdraw})
    (get-account-summary node account-id)))

(defn transfer-money [node sender-id recipient-id amount]
  (when (db/sync-transfer! node {:sender-id sender-id
                                 :recipient-id recipient-id
                                 :amount amount
                                 :kind :transfer})
    (get-account-summary node sender-id)))

;; audit-log (public)

(defn- transactors [log-entry]
  (-> log-entry
      :xtdb.api/doc
      ::db/tx-args
      first ; should schematize audit entries and get this with key
      (select-keys [:sender-id :recipient-id])))

(defn- participant? [account-id log-entry]
  (->> log-entry
       transactors
       vals
       (some #(= account-id %))
       boolean))

(defn- format-entry [account-id idx log-entry]
  (let [role ({:sender-id :sender
               :recipient-id :recipient} (participant? account-id log-entry))]
    (-> {:sequence idx}
        (assoc (case role
                 :sender :debit
                 :recipient :credit)
               (get-in log-entry [:xtdb.api/doc ::db/tx-args 0 :amount]))
        (assoc :description
               (match [role (get-in log-entry [:xtdb.api/doc ::db/tx-kind])]
                 [_ :withdraw] "withdraw"
                 [_ :deposit] "deposit"
                 [:sender :transfer] (str "send to "
                                          (:recipient-id (transactors log-entry)))
                 [:recipient :transfer] (str "receive from "
                                             (:sender-id (transactors log-entry))))))))

;; In a real world scenario we should have the ability retrieve partial audit logs based on indexers
(defn audit-log [node account-id & args]
  (->> (apply db/full-audit-log! node args)
       (sequence (comp (filter (partial participant? account-id))
                       (map-indexed (partial format-entry account-id))))
       reverse))
